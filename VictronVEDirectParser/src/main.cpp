#include <Arduino.h>
#include <stdio.h>
#include <stdlib.h>

#define victronSerial Serial3   // Change it as needed

unsigned char incomingByte = 0; 
unsigned char victronFieldLabel[20];
unsigned int victronFieldLabelIndex = 0;
unsigned char victronFieldValue[20];
unsigned int victronFieldValueIndex = 0;

enum victronPacketState
{
  _IDLE,
  _RECORD_LABEL,
  _RECORD_VALUE,
};
victronPacketState mState = _IDLE;

unsigned int soc_value = 0;

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(9600);   // For debug
  victronSerial.begin(19200); // For the SmartSHunt
}

void loop()
{
  // Check if data is available
  if (victronSerial.available())
  {
    // Read the data
    incomingByte = Serial.read();

    // State machine
    switch (mState)
    {
    case _IDLE:
      switch (incomingByte)
      {
      /* wait for \n of the start of an record */
      case 0x0A:
        mState = _RECORD_LABEL;
        break;

      default:
        break;
      }
      break;

    case _RECORD_LABEL:
      switch (incomingByte)
      {
      /* wait for \t of the end of the label */
      case 0x09:
        mState = _RECORD_VALUE;
        victronFieldLabel[victronFieldLabelIndex] = '\0';
        break;

      case 0x0D:
        mState = _IDLE;
        victronFieldLabelIndex = 0;
        victronFieldValueIndex = 0;
        break;

      /* Record the label */
      default:
        victronFieldLabel[victronFieldLabelIndex] = incomingByte;
        victronFieldLabelIndex++;
        break;
      }
      break;

    case _RECORD_VALUE:
      switch (incomingByte)
      {
      /* wait for \r of the end of an record */
      case 0x0D:
        victronFieldValue[victronFieldValueIndex] = '\0';

        // Check if the label is the one we want
        if (strcmp(victronFieldLabel, "SOC") == 0)
        {
          soc_value = atoi(victronFieldValue);
          Serial.println(soc_value, DEC);
        }
        else
        {
          Serial.println("Not my label!");
        }
        // Reset the state
        mState = _IDLE;
        victronFieldLabelIndex = 0;
        victronFieldValueIndex = 0;
        break;

      /* Record the value */
      default:
        victronFieldValue[victronFieldValueIndex] = incomingByte;
        victronFieldValueIndex++;
        break;
      }
      break;

    default:
      mState = _IDLE;
      victronFieldLabelIndex = 0;
      victronFieldValueIndex = 0;
      break;
    }
  }
}